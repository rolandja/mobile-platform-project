import axios from 'axios';

export default () => {
    let headers = {
        'cache-control': 'no-cache'
    };
    let accessToken = window.$cookies.get("token")
    if (accessToken && accessToken !== '') {
        headers.auth = accessToken
    }
    return axios.create({
        baseURL: process.env.VUE_APP_BASE_URL,
        headers: headers
    });
}