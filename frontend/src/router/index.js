import Vue from 'vue'
import Router from "vue-router"

import Home from '../views/Home'
import Login from '../views/Login'
import Register from '../views/Register'
import NotFound from '../views/NotFound'
import Dashboard from '../views/Dashboard'
import Charts from '../views/Charts'

import Admin from '../views/Admin'

import axios from "../services/api"

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            meta: {
                guest: true
            },
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
            meta: {
                guest: true
            },
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                authRequired: true
            },
        },
        {
            path: '/charts',
            name: 'Charts',
            component: Charts,
            meta: {
                authRequired: true
            },
        },
        {
            path: '/admin',
            name: 'Admin',
            component: Admin,
            meta: {
                authRequired: true
            },
        },
        {
            path: '*',
            name: 'NotFound',
            component: NotFound
        }


    ],
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.guest)) {
        if (!Vue.$cookies.get("token")) {
            next();
        } else {
            axios()
                .post('/api/verify')
                .then(() => {
                    next("/")
                })
                .catch(() => {
                    next()
                })
        }
    } else if (to.matched.some(record => record.meta.authRequired)) {
        if (!Vue.$cookies.get("token")) {
            next("/");
        } else {
            axios()
                .post('/api/verify')
                .then(() => {
                    next()
                })
                .catch(() => {
                    next("/")
                })
        }
    } else {
        next()
    }
})


export default router