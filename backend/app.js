let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let app = express();
let cors = require('cors');
require('dotenv').config()

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var corsOptions = {
	origin: '*',
}

app.use(cors(corsOptions));

const mongoUtil = require('./database/mongoUtil');
mongoUtil.connectToServer((err, client) => {
	if (err) console.log(err);
	// start the rest of your app here
	let indexRouter = require('./routes/index');
	let sensorRouter = require('./routes/sensor');
	let sensorDataRouter = require('./routes/sensorData');
	let apiRouter = require('./routes/api');

	app.use('/', indexRouter);
	app.use('/api', apiRouter);
	app.use('/api/sensor', sensorRouter);
	app.use('/api/sensorData', sensorDataRouter);
});

module.exports = app;