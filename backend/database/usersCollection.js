const mongoUtil = require('./mongoUtil');
let DB = mongoUtil.getDB();
let col = DB.collection('user');

// Makes name unique
col.createIndex({ 'username': 1 }, { unique: true });

module.exports = {
	createUser: async (user) => {
		user.role = "user"
		await col.insertOne(user);
	},
	findOne: async (search) => {
		const data = await col.findOne(search);
		return data;
	},
}