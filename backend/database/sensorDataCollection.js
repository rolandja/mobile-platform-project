const mongoUtil = require('./mongoUtil');
let DB = mongoUtil.getDB();
let collection = DB.collection('sensor-data');

module.exports = {
	saveMany: async (data) => {
		return await collection.insertMany(data);
	},
	getAll: async (search = {}) => {
		let data = []
		await collection.find(search).sort({ _id: -1 }).limit(50).forEach(doc => {
			doc.timeStamp = doc._id.getTimestamp()
			data.push(doc)
		});
		return data
	},
	getChartType: async (search) => {
		let data = []
		await collection.find(search).sort({ _id: -1 }).limit(50).forEach((doc) => {
			data.push({ value: doc.value, date: doc._id.getTimestamp() })
		});
		return data
	},
}