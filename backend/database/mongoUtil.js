const { MongoClient } = require('mongodb');
const { ObjectId } = require('mongodb');

let DB;

const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;

module.exports = {

  connectToServer: function (callback) {
    const url = 'mongodb+srv://' + DB_USERNAME + ':' + DB_PASSWORD + '@mobile-platform-cluster.0rmmi.mongodb.net/' + DB_NAME + '?retryWrites=true&w=majority';
    MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
      DB = client.db(DB_NAME);
      return callback(err);
    });
  },

  getDB: function () {
    return DB;
  },

  getObjectId: function () {
    return ObjectId;
  },
};