const mongoUtil = require('./mongoUtil');
const ObjectId = mongoUtil.getObjectId();
let DB = mongoUtil.getDB();
let col = DB.collection('sensor');

// Makes name unique
col.createIndex({ 'name': 1 }, { unique: true });

module.exports = {
	createSensor: async (sensor) => {
		sensor.addedOn = new Date();
		sensor.started = false;
		return await col.insertOne(sensor);
	},
	startSensor: async (sensorName) => {
		await col.updateOne({ name: sensorName }, { $set: { started: true } });
	},
	stopSensor: async (sensorName) => {
		await col.updateOne({ name: sensorName }, { $set: { started: false } });
	},
	getAll: async () => {
		return await col.find().toArray();
	},
	find: async (search) => {
		return await col.find(search).toArray();
	},
	delete: async (sensorId) => {
		return await col.deleteOne({ _id: ObjectId(sensorId) });
	},
}