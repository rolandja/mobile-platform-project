let express = require('express');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

let router = express.Router();
let usersCollection = require('../database/usersCollection');
let tokenUtil = require('../util/verifyToken');

router.post('/register', async function (req, res, next) {
    const SALT = await bcrypt.genSalt(10);
    const PASSWORD = await bcrypt.hash(req.body.password, SALT);

    try {
        let data = await usersCollection.createUser({ username: req.body.username, password: PASSWORD })
        res.sendStatus(200);
    } catch (err) {
        if (err.code == 11000) {
            res.status(409).send()
        } else {
            res.sendStatus(400);
        }
    }
});

router.post('/login', async function (req, res, next) {
    try {
        let user = await usersCollection.findOne({ username: req.body.username });
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) {
            res.status(400).send('Password does not match')
        } else {

            const token = jwt.sign(
                // payload data
                {
                    id: user._id,
                    username: user.username,
                    role: user.role
                },
                process.env.TOKEN_SECRET
            );
            res.header("auth-token", token).json({ id: user._id, username: user.username, role: user.role, token: token });
        }
    } catch (err) {
        console.log(err)
        res.sendStatus(404);
    }
});

router.post('/verify', tokenUtil.verify, function (req, res, next) {
    try {
        res.status(200).json(req.user);
    } catch (err) {
        res.sendStatus(404);
    }
});


module.exports = router;
