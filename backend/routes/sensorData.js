let express = require('express');
let router = express.Router();
let sensorDataCollection = require('../database/sensorDataCollection');

router.post('/', async function (req, res, next) {
    try {
        let data = await sensorDataCollection.saveMany(req.body)
        res.sendStatus(200);
    } catch (err) {
        console.log(err.errmsg)
        res.sendStatus(404);
    }
});

router.get('/', async function (req, res, next) {
    try {
        let data = await sensorDataCollection.getAll()
        res.status(200).send(data);
    } catch (err) {
        console.log(err)
        res.sendStatus(404);
    }
});

router.get('/search/:searchKey/:searchValue', async function (req, res, next) {
    let searchString = {}
    searchString[req.params.searchKey] = req.params.searchValue
    try {
        let data = await sensorDataCollection.getAll(searchString)
        res.status(200).send(data);
    } catch (err) {
        res.status(err.code).send(err.errmsg);
    }
});

router.get('/chart/type/:type', async function (req, res, next) {
    try {
        let data = await sensorDataCollection.getChartType({ type: req.params.type})
        res.status(200).send(data);
    } catch (err) {
        console.log(err)
        res.sendStatus(404);
    }
});

// router.get('/chart/co2', async function (req, res, next) {
//     try {
//         let data = await sensorDataCollection.getChartCo2()
//         res.status(200).send(data);
//     } catch (err) {
//         console.log(err)
//         res.sendStatus(404);
//     }
// });

module.exports = router;
