let express = require('express');
let axios = require('axios');
let router = express.Router();
let sensorCollection = require('../database/sensorCollection');
let tokenUtil = require('../util/verifyToken');
const { token } = require('morgan');

router.get('/', tokenUtil.verify, async function (req, res, next) {
	try {
		let data = await sensorCollection.getAll()
		res.send(data);
	} catch (err) {
		res.status(404).send(err.errmsg);
	}
});

router.post('/', tokenUtil.verifyAdmin, async function (req, res, next) {
	try {
		let sensor = {
			name: req.body.sensorName,
			scheduledDataTransferCronJob: '* * * * * * * * ',
		};
		let data = await sensorCollection.createSensor(sensor)
		res.sendStatus(200);
	} catch (err) {
		res.status(404).send(err.errmsg);
	}
});

router.delete('/:sensorId', tokenUtil.verifyAdmin, async function (req, res, next) {
	try {
		let data = await sensorCollection.delete(req.params.sensorId)
		res.status(200).send(data);
	} catch (err) {
		res.status(err.code).send(err.errmsg);
	}
});

router.get('/search/:sensorName', tokenUtil.verify, async function (req, res, next) {
	try {
		let data = await sensorCollection.find({ name: req.params.sensorName });
		res.send(data);
	} catch (err) {
		res.status(err.code).send(err.errmsg);
	}
});
process.env.SENSOR_URL
router.post('/start', tokenUtil.verifyAdmin, async function (req, res, next) {
	await axios.post(process.env.SENSOR_URL + '/api/sensor/start', { sensorName: req.body.sensorName })
		.then((response) => {
			console.log('started')
			sensorCollection.startSensor(req.body.sensorName);
			res.sendStatus(200);
		})
		.catch((error) => {
			res.sendStatus(400)
		})
});

router.post('/stop', tokenUtil.verifyAdmin, async function (req, res, next) {
	await axios.post(process.env.SENSOR_URL + '/api/sensor/stop', { sensorName: req.body.sensorName })
		.then((response) => {
			sensorCollection.stopSensor(req.body.sensorName);
			res.sendStatus(200);
		})
		.catch((error) => {
			console.log(console.error())
			res.sendStatus(400)
		})
});

module.exports = router;
