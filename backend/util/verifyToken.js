const jwt = require("jsonwebtoken");

module.exports = {
    verify: function (req, res, next) {
        if (req.header("secret")) {
            next();
            return;
        }
        const token = req.header("auth");
        if (!token) return res.status(401).json({ error: "Access denied" });
        try {
            const verified = jwt.verify(token, process.env.TOKEN_SECRET);
            req.user = verified;
            next();
        } catch (err) {
            res.status(400).json({ error: "Token is not valid" });
        }
    },
    verifyAdmin: function (req, res, next) {
        if (req.header("secret")) {
            next();
            return;
        }
        const token = req.header("auth");
        if (!token) return res.status(401).json({ error: "Access denied" });
        try {
            const verified = jwt.verify(token, process.env.TOKEN_SECRET);
            if (verified.role !== 'admin') {
                return res.status(401).json({ error: "Access denied" });
            }
            req.user = verified;
            next();
        } catch (err) {
            res.status(400).json({ error: "Token is not valid" });
        }
    },
}
