# mobile-platform-project
```
Frontend was deployed in firebase (no need to serve build files with express)
    - simply follow firebase deploy
Backend and sensor were deployed to heroku (firebase gave alot of trouble for express)
Since everything is under one git, some special things need to be done to deploy to heroku:
    1. every single time you need to set the correct heroku remote
    2. git subtree push --prefix path/to/subdirectory heroku master (from the root of project) 
    2.1 git subtree push --prefix backend heroku master
    2.2 git subtree push --prefix sensor-generator-project heroku master
Keep in mind that URL's need to be changed accordingly with what you get

    - frontend - https://mobile-project-frontend.web.app/
    - backend - https://mobile-platforms-backend.herokuapp.com/
    - sensor - https://mobile-platforms-sensor.herokuapp.com/
```

## 3 project setup
```
frontend ( vue, bootstrap, amCharts )
backend ( node, express, mongo, bcrypt, jwt )
sensor-generator ( node, express )
```
### How to setup Locally

1. Clone the project
2. npm install each project
3. Create .env files in each project and replace values accordingly
    - frontend\
    ```
    VUE_APP_BASE_URL = <url>
    ```
    - backend\
    ```
    DB_USERNAME = <username>
    DB_PASSWORD = <password>
    DB_NAME = <db name>
    TOKEN_SECRET = <jwt secret>
    SENSOR_URL = <sensor project url>
    ```
    - sensor-generator
    ```
    BASE_URL = <url>
    ```
4. run projects
    - frontend ` npm serve `
    - backend ` npm start `
    - sensor-generator ` npm start `

### Things to know
    All users created are of role 'user', 'admin' is only changed programatically
    All users see the same data
    Admin can manipulate sensors



