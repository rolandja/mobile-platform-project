let axios = require('axios');
let cron = require('node-cron');

let sensors = [];

let self = module.exports = {
    getSensorsFromDB: async function () {
        console.log('get sensor names')
        await axios.get(process.env.BASE_URL + '/api/sensor', {
            headers: {
                secret: true,
            }
        })
            .then((response) => {
                // handle success
                sensors = response.data;
            })
            .catch((error) => {
                // handle error
                console.log(error.response);
            })
        return sensors;
    },

    saveSensorData: function (data) {
        axios.post(process.env.BASE_URL + '/api/sensorData', data)
            .then((response) => {
                // console.log(response);
            })
            .catch((error) => {
                // handle error
                console.log(error.response.statusCode);
            })
    },

    generateDataTask: function (generateTemperature, generateCo2) {
        cron.schedule('0 */10 * * * *', () => {
            console.log('generating data')
            let temperatureObj
            let co2Obj
            let sensData = [];
            sensors.filter((sensor) => sensor.started)
                .forEach((sensor) => {
                    sensData = [];
                    temperatureObj = generateTemperature();
                    co2Obj = generateCo2();
                    temperatureObj.sensor = sensor.name;
                    co2Obj.sensor = sensor.name;
                    sensData.push(temperatureObj);
                    sensData.push(co2Obj);
                    if (temperatureObj.event === 'Warning' || co2Obj.event === 'Warning') {
                        console.log('warning save to db')
                        self.saveSensorData(sensData);
                    }
                })
        });
    },

    scheduledTask: function (generateTemperature, generateCo2) {
        // */10 * * * * * >> every 10 seconds
        // 0 */10 * * * * >> every 10 minutes
        // 0 0 * * * * >> every hour
        // 0 0 */4 * * * >> every 4 hours
        cron.schedule('0 0 */2 * * *', () => {
            console.log('scheduled task')
            let temperatureObj;
            let co2Obj;
            let sensData = [];
            sensors.filter((sensor) => sensor.started)
                .forEach((sensor) => {
                    temperatureObj = generateTemperature();
                    co2Obj = generateCo2();
                    temperatureObj.sensor = sensor.name;
                    co2Obj.sensor = sensor.name;
                    sensData.push(temperatureObj);
                    sensData.push(co2Obj);
                });
            self.saveSensorData(sensData);
        });
    },

    startSensor: function (sensorName) {
        console.log('trying to start')
        sensors
            .filter((sensor) => sensor.name === sensorName)
            .forEach((sensor) => sensor.started = true)
    },

    stopSensor: function (sensorName) {
        console.log('trying to stop')
        sensors
            .filter((sensor) => sensor.name === sensorName)
            .forEach((sensor) => sensor.started = false)
    },
}