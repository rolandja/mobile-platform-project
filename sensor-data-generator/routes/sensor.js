let express = require('express');
let router = express.Router();
// let sensorUtil = require('../sensor/sensorUtil');

module.exports = (sensorUtil) => {
	router.get('/', function (req, res, next) {
		res.send('respond with a resource');
	});

	router.post('/start', function (req, res, next) {
		sensorUtil.startSensor(req.body.sensorName);
		res.sendStatus(200)
	});

	router.post('/stop', function (req, res, next) {
		sensorUtil.stopSensor(req.body.sensorName);
		res.sendStatus(200)
	});
	return router;
}
