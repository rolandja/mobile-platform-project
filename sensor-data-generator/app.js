let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let app = express();
let cors = require('cors')
require('dotenv').config()

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var corsOptions = {
    origin: 'http://localhost:8080',
}

app.use(cors(corsOptions));

let countTempLow = 0;
let countTempHigh = 0;
function generateTemperature() {
    let min = Math.ceil(10);
    let max = Math.floor(30);
    const tempValue = Math.floor(Math.random() * (max - min + 1) + min)
    if (tempValue < 13) {
        countTempLow++;
    } else if (tempValue > 28) {
        countTempHigh++;
    } else {
        countTempLow = 0;
        countTempHigh = 0;
    }
    if (countTempLow === 2) {
        countTempLow = 0;
        return { type: 'temperature', value: tempValue, event: 'Warning', message: 'Low temperature' };
    }
    if (countTempHigh === 2) {
        countTempHigh = 0;
        return { type: 'temperature', value: tempValue, event: 'Warning', message: 'High temperature' };
    }
    return { type: 'temperature', value: tempValue, event: 'Normal', message: '' };
}


let countCo2 = 0;
function generateCo2() {
    let min = Math.ceil(600);
    let max = Math.floor(7000);
    const co2Value = Math.floor(Math.random() * (max - min + 1) + min)
    if (co2Value > 6000) {
        countCo2++;
    } else {
        countCo2 = 0;
    }
    if (countCo2 === 2) {
        countCo2 = 0;
        return { type: 'co2', value: co2Value, event: 'Warning', message: 'High Co2 concentration' };
    }
    return { type: 'co2', value: co2Value, event: 'Normal', message: '' };
}

let sensorUtil = require('./sensor/sensorUtil');
sensorUtil.getSensorsFromDB();
sensorUtil.generateDataTask(generateTemperature, generateCo2);
sensorUtil.scheduledTask(generateTemperature, generateCo2);

let indexRouter = require('./routes/index');
let sensorRouter = require('./routes/sensor')(sensorUtil);

app.use('/', indexRouter);
app.use('/api/sensor', sensorRouter);

module.exports = app;
